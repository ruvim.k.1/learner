/*
 * File: Util.java
 * Author: Ruvim Kondratyev
 * Date: Sat, 17 Jul 2021
 *
 * Copyright 2021 by Ruvim Kondratyev.
 *
 * Written on Saturday, July 17, 2021.
 * The point of this was to create a
 * demo that can be used for applying
 * to a software job with a company
 * that writes eLearning software and
 * uses SCORM, xAPI, and other
 * standards as well as helps its
 * customers adopt the standards.
 *
 */
package com.gradgoose.learner;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * User story:
 * As a developer, I need the common utility functions to be in their own class so that they're reusable.
 * 
 * This utility class implements that user story.
 * 
 */
public class Util
{
	/**
	 * The tag used for logging.
	 */
	static final String TAG = "Util";

	/**
	 * Does nothing because this is a utility class.
	 */
	private Util ()
	{
		// Utility class. Not allowed to instantiate it!
	}

	/**
	 * Course content.
	 */
	public static class Course
	{
		/**
		 * Course name.
		 */
		public String name = "";
		/**
		 * List of lessons in this course.
		 */
		public Vector<Lesson> lessons = new Vector<> ();

		/**
		 * Finds an item (book or quiz) across lessons based on absolute position within the course.
		 * 
		 * @param position The item's position within the course.
		 * @return The item (an instance of Book or Quiz, depending on the item), or null if not found.
		 */
		public Object findItem (int position)
		{
			int cumulative = 0;
			for (int i = 0; i < lessons.size (); i++)
			{
				Util.Lesson lesson = lessons.get (i);
				int nBooks = lesson.books.size ();
				int nQuizzes = lesson.quizzes.size ();
				if (position >= cumulative && position < cumulative + nBooks)
				{
					return lesson.books.get (position - cumulative);
				}
				cumulative += nBooks;
				if (position >= cumulative && position < cumulative + nQuizzes)
				{
					return lesson.quizzes.get (position - cumulative);
				}
				cumulative += nQuizzes;
			}
			return null;
		}

		/**
		 * Counts the items in this course.
		 * 
		 * @return Total item count in the course.
		 */
		public int countItems ()
		{
			int total = 0;
			for (int i = 0; i < lessons.size (); i++)
			{
				Util.Lesson lesson = lessons.get (i);
				total += lesson.books.size () + lesson.quizzes.size ();
			}
			return total;
		}
	}

	/**
	 * Contains the course content for one lesson of the course.
	 */
	public static class Lesson
	{
		/**
		 * Lesson name.
		 */
		public String name = "";
		/**
		 * Books in this lesson.
		 * 
		 * Here, "book" just means "assigned reading material,"
		 * so it's not a "textbook" in that sense but rather
		 * a unit of reading material that has chapters and is
		 * assignable as reading homework for a lesson.
		 * 
		 */
		public Vector<Book> books = new Vector<> ();
		/**
		 * Quizzes in this lesson.
		 */
		public Vector<Quiz> quizzes = new Vector<> ();
	}

	/**
	 * A unit of reading material that is assignable as reading homework for a lesson.
	 */
	public static class Book
	{
		/**
		 * Book name.
		 */
		public String name = "";
		/**
		 * Chapters in this book.
		 */
		public Vector<BookChapter> chapters = new Vector<> ();
	}

	/**
	 * Chapter in a book.
	 * 
	 * Or, chapter in a unit of reading material for a lesson.
	 * 
	 * Or just chapter.
	 * 
	 */
	public static class BookChapter
	{
		/**
		 * Chapter name.
		 */
		public String name = "";
		/**
		 * Pages in this chapter.
		 */
		public Vector<BookPage> pages = new Vector<> ();
	}

	/**
	 * One page of reading material.
	 * 
	 * Contains a short text and an image.
	 * 
	 * If the image is missing or corrupted,
	 * then it will not be shown on the UI,
	 * so this would effectively be just
	 * text without an image.
	 * 
	 */
	public static class BookPage
	{
		public String text = "";
		public String img = "";
	}

	/**
	 * Quiz for a lesson.
	 */
	public static class Quiz
	{
		/**
		 * Quiz name.
		 */
		public String name = "";
		/**
		 * Questions on this quiz.
		 */
		public Vector<QuizQuestion> questions = new Vector<> ();

		/**
		 * Randomizes the questions in the quiz.
		 * 
		 * For the algorithm, uses an O(N) algorithm
		 * that performs swap operations, swapping
		 * random items to the back of the list until
		 * it traverses the entire list.
		 * 
		 * This is better than a random "sort" because
		 * the sort would take at least O(N log N) or
		 * greater.
		 * 
		 */
		public void randomizeQuestions ()
		{
			for (int i = questions.size (); i > 1; i--)
			{
				int a = i - 1;
				int b = (int) Math.floor (i * Math.random ());
				QuizQuestion tmp = questions.get (a);
				questions.set (a, questions.get (b));
				questions.set (b, tmp);
			}
		}

		/**
		 * Randomizes both questions and answer options.
		 */
		public void randomize ()
		{
			for (QuizQuestion question : questions)
			{
				question.randomizeAnswerOptions ();
			}
			randomizeQuestions ();
		}

		/**
		 * Grades this quiz.
		 * 
		 * @return A GradeReport. See docs for GradeReport for details.
		 */
		public GradeReport grade ()
		{
			GradeReport report = new GradeReport ();
			for (QuizQuestion question : questions)
			{
				boolean correct = question.checkAnswer ();
				if (correct)
				{
					report.earned += question.weight;
				}
				report.max += question.weight;
			}
			report.scaled = report.earned / report.max;
			return report;
		}
	}

	/**
	 * Question on a quiz.
	 */
	public static class QuizQuestion
	{
		/**
		 * Weight of this question (how many points does this question earn?).
		 */
		public float weight = 1.0f;
		/**
		 * Type of this question.
		 * 
		 * Can be multiple choice (single answer) or multiple answer.
		 * 
		 * See QuestionType.
		 * 
		 */
		public QuestionType type = QuestionType.SINGLE_ANSWER;
		/**
		 * Text of the question.
		 */
		public String text = "";
		/**
		 * Answer options.
		 */
		public Vector<AnswerOption> options = new Vector<> ();

		/**
		 * Randomizes the answer options for this question.
		 * 
		 * Same algorithm as the one to randomize questions.
		 *
		 * For the algorithm, uses an O(N) algorithm
		 * that performs swap operations, swapping
		 * random items to the back of the list until
		 * it traverses the entire list.
		 *
		 * This is better than a random "sort" because
		 * the sort would take at least O(N log N) or
		 * greater.
		 *
		 */
		public void randomizeAnswerOptions ()
		{
			for (int i = options.size (); i > 1; i--)
			{
				int a = i - 1;
				int b = (int) Math.floor (i * Math.random ());
				AnswerOption tmp = options.get (a);
				options.set (a, options.get (b));
				options.set (b, tmp);
			}
		}

		/**
		 * Checks the user's answer.
		 * 
		 * @return True if the user answered correctly, or false otherwise.
		 */
		public boolean checkAnswer ()
		{
			boolean allCorrect = true;
			for (AnswerOption option : options)
			{
				allCorrect &= (option.correct == option.userSelected);
			}
			return allCorrect;
		}
	}

	/**
	 * Answer option for a question on the quiz.
	 */
	public static class AnswerOption
	{
		/**
		 * Whether or not this answer option is a correct option.
		 * 
		 * That is, this is the answer key's value on whether this should be chosen as an answer.
		 * 
		 */
		public boolean correct = false;      // The answer key value.
		/**
		 * Whether or not this answer option was selected by the user.
		 * 
		 * In other words, this is what the user's response is to this option.
		 * 
		 */
		public boolean userSelected = false; // The user-selected value.
		/**
		 * Text of the answer option.
		 */
		public String text = "";
	}

	/**
	 * Type of the question.
	 * 
	 */
	public static enum QuestionType
	{
		/**
		 * Single-answer questions are ones where there are radio buttons
		 * and only one possible answer.
		 */
		SINGLE_ANSWER,
		/**
		 * Multi-select questions are ones where there are checkboxes
		 * and multiple possible answers ("select all that apply").
		 */
		MULTI_SELECT
	}

	/**
	 * Grade report for the quiz.
	 */
	public static class GradeReport
	{
		/**
		 * Minimum score possible.
		 */
		public float min = 0.0f;
		/**
		 * Maximum score possible.
		 */
		public float max = 0.0f;
		/**
		 * Raw score earned by the user.
		 */
		public float earned = 0.0f;
		/**
		 * User's score scaled between 0 and 1.
		 * 
		 * For this demo, earned/max was used,
		 * but an alternative equation of (earned-min)/(max-min)
		 * is also possible depending on standards.
		 * 
		 */
		public float scaled = 0.0f;
	}

	/**
	 * Loads a course from a file.
	 * 
	 * This function uses the W3C DOM as well as ZipFile to
	 * load course contents. A course file is just a ZIP file
	 * with the .course filename extension, so we do that.
	 * 
	 * @param courseFile The course file.
	 * @return The course.
	 */
	public static Course loadCourse (File courseFile)
	{
		Course result = new Course ();
		try
		{
			ZipFile zipFile = new ZipFile (courseFile);
			ZipEntry entry = zipFile.getEntry ("index.xml");
			if (entry != null)
			{
				InputStream inputStream = zipFile.getInputStream (entry);
				try
				{
					Document doc = DocumentBuilderFactory.newInstance ().newDocumentBuilder ().parse (inputStream);
					NodeList elems = doc.getElementsByTagName ("course");
					if (elems.getLength () > 0)
					{
						Node course = elems.item (0);
						Node nameAttr = course.getAttributes ().getNamedItem ("name");
						if (nameAttr != null)
						{
							String value = nameAttr.getTextContent ();
							Log.i (TAG, "Found course name to be: " + value);
							result.name = value;
						}
						NodeList lessons = doc.getElementsByTagName ("lesson");
						for (int i = 0; i < lessons.getLength (); i++)
						{
							Node lessonNode = lessons.item (i);
							Lesson lesson = new Lesson ();
							nameAttr = lessonNode.getAttributes ().getNamedItem ("name");
							if (nameAttr != null)
							{
								lesson.name = nameAttr.getTextContent ();
							}
							NodeList children = lessonNode.getChildNodes ();
							for (int j = 0; j < children.getLength (); j++)
							{
								Node child = children.item (j);
								if (child.getNodeName ().equals ("books"))
								{
									NodeList subChildren = child.getChildNodes ();
									for (int k = 0; k < subChildren.getLength (); k++)
									{
										Node subChild = subChildren.item (k);
										if (subChild.getNodeName ().equals ("book"))
										{
											lesson.books.add (loadBook (zipFile, subChild.getTextContent ()));
										}
									}
								}
								else if (child.getNodeName ().equals ("quizzes"))
								{
									NodeList subChildren = child.getChildNodes ();
									for (int k = 0; k < subChildren.getLength (); k++)
									{
										Node subChild = subChildren.item (k);
										if (subChild.getNodeName ().equals ("quiz"))
										{
											lesson.quizzes.add (loadQuiz (zipFile, subChild.getTextContent ()));
										}
									}
								}
							}
							result.lessons.add (lesson);
						}
					}
				}
				catch (ParserConfigurationException e)
				{
					Log.e (TAG, "Parse configuration exception: " + e.getMessage ());
				}
				catch (SAXException e)
				{
					Log.e (TAG, "SAX exception: " + e.getMessage ());
				}
			}
			else
			{
				Log.e (TAG, "Course file does not contain index!");
			}
			zipFile.close ();
		}
		catch (IOException e)
		{
			Log.e (TAG, "Error handling the course file: " + e.getMessage ());
		}
		return result;
	}

	/**
	 * Loads a book from a course.
	 * 
	 * @param zipFile ZIP file to load the book from.
	 * @param bookPath Path to the book within the ZIP file.
	 * @return The book.
	 */
	private static Book loadBook (ZipFile zipFile, String bookPath)
	{
		Log.i (TAG, "Loading book " + bookPath);
		Book book = new Book ();
		ZipEntry entry = zipFile.getEntry (bookPath);
		if (entry != null)
		{
			try
			{
				InputStream inputStream = zipFile.getInputStream (entry);
				try
				{
					Document doc = DocumentBuilderFactory.newInstance ().newDocumentBuilder ().parse (inputStream);
					NodeList bookNodes = doc.getElementsByTagName ("book");
					if (bookNodes.getLength () > 0)
					{
						Node bookNode = bookNodes.item (0);
						Node nameAttr = bookNode.getAttributes ().getNamedItem ("name");
						if (nameAttr != null)
						{
							book.name = nameAttr.getTextContent ();
						}
						NodeList bookChildren = bookNode.getChildNodes ();
						for (int i = 0; i < bookChildren.getLength (); i++)
						{
							Node bookChild = bookChildren.item (i);
							if (bookChild.getNodeName ().equals ("chapter"))
							{
								book.chapters.add (loadChapter (zipFile, bookPath, bookChild));
							}
						}
					}
				}
				catch (ParserConfigurationException e)
				{
					Log.e (TAG, "Parse configuration exception: " + e.getMessage ());
				}
				catch (SAXException e)
				{
					Log.e (TAG, "SAX exception: " + e.getMessage ());
				}
			}
			catch (IOException err)
			{
				Log.e (TAG, "Error reading ZIP entry for " + bookPath);
			}
		}
		else
		{
			Log.e (TAG, "Could not find ZIP entry for " + bookPath);
		}
		return book;
	}

	/**
	 * Loads a chapter in a book.
	 * 
	 * @param zipFile ZIP file for the course.
	 * @param bookPath Path to the book within the ZIP file.
	 * @param chapterNode DOM node with the chapter to load.
	 * @return The chapter.
	 */
	static BookChapter loadChapter (ZipFile zipFile, String bookPath, Node chapterNode)
	{
		BookChapter chapter = new BookChapter ();
		Node nameAttr = chapterNode.getAttributes ().getNamedItem ("name");
		if (nameAttr != null)
		{
			chapter.name = nameAttr.getTextContent ();
		}
		NodeList chapterChildren = chapterNode.getChildNodes ();
		for (int i = 0; i < chapterChildren.getLength (); i++)
		{
			Node chapterChild = chapterChildren.item (i);
			if (chapterChild.getNodeName ().equals ("page"))
			{
				chapter.pages.add (loadPage (zipFile, bookPath, chapterChild));
			}
		}
		return chapter;
	}

	/**
	 * Loads a page from a book.
	 * 
	 * @param zipFile ZIP file for the course.
	 * @param bookPath Path to the book within the ZIP file.
	 * @param pageNode DOM node with the page to load.
	 * @return The page.
	 */
	static BookPage loadPage (ZipFile zipFile, String bookPath, Node pageNode)
	{
		BookPage page = new BookPage ();
		NodeList children = pageNode.getChildNodes ();
		for (int i = 0; i < children.getLength (); i++)
		{
			Node child = children.item (i);
			if (child.getNodeName ().equals ("text"))
			{
				page.text = child.getTextContent ();
			}
			else if (child.getNodeName ().equals ("img"))
			{
				String rawUri = child.getTextContent ();
				if (zipFile.getEntry (rawUri) != null)
				{
					Log.i (TAG, "Found image " + rawUri);
					page.img = rawUri;
				}
				else
				{
					String tmp = new File (bookPath).getParent ().toString () + "/" + rawUri;
					if (zipFile.getEntry (tmp) != null)
					{
						Log.i (TAG, "Found image " + tmp);
						page.img = tmp;
					}
					else
					{
						Log.w (TAG, "Image not found (" + bookPath + "; " + rawUri + ")");
					}
				}
			}
		}
		return page;
	}

	/**
	 * Loads a quiz from a course file.
	 * 
	 * @param zipFile The ZIP file with the course.
	 * @param quizPath The path to the quiz within the ZIP file.
	 * @return The quiz.
	 */
	private static Quiz loadQuiz (ZipFile zipFile, String quizPath)
	{
		Log.i (TAG, "Loading quiz " + quizPath);
		Quiz quiz = new Quiz ();
		ZipEntry entry = zipFile.getEntry (quizPath);
		if (entry != null)
		{
			try
			{
				InputStream inputStream = zipFile.getInputStream (entry);
				try
				{
					Document doc = DocumentBuilderFactory.newInstance ().newDocumentBuilder ().parse (inputStream);
					NodeList quizNodes = doc.getElementsByTagName ("quiz");
					if (quizNodes.getLength () > 0)
					{
						Node quizNode = quizNodes.item (0);
						Node nameAttr = quizNode.getAttributes ().getNamedItem ("name");
						if (nameAttr != null)
						{
							quiz.name = nameAttr.getTextContent ();
						}
						NodeList quizChildren = quizNode.getChildNodes ();
						for (int i = 0; i < quizChildren.getLength (); i++)
						{
							Node quizChild = quizChildren.item (i);
							if (quizChild.getNodeName ().equals ("question"))
							{
								quiz.questions.add (loadQuestion (zipFile, quizPath, quizChild));
							}
						}
					}
				}
				catch (ParserConfigurationException e)
				{
					Log.e (TAG, "Parse configuration exception: " + e.getMessage ());
				}
				catch (SAXException e)
				{
					Log.e (TAG, "SAX exception: " + e.getMessage ());
				}
			}
			catch (IOException err)
			{
				Log.e (TAG, "Error reading ZIP entry for " + quizPath);
			}
		}
		else
		{
			Log.e (TAG, "Could not find ZIP entry for " + quizPath);
		}
		return quiz;
	}

	/**
	 * Loads a question for a quiz.
	 * 
	 * @param zipFile The ZIP file for the course.
	 * @param quizPath The path to the quiz within the ZIP file.
	 * @param questionNode DOM node with the question to load.
	 * @return The question.
	 */
	private static QuizQuestion loadQuestion (ZipFile zipFile, String quizPath, Node questionNode)
	{
		QuizQuestion question = new QuizQuestion ();
		Node weightAttr = questionNode.getAttributes ().getNamedItem ("weight");
		if (weightAttr != null)
		{
			try
			{
				question.weight = Float.parseFloat (weightAttr.getTextContent ());
			}
			catch (NumberFormatException err)
			{
				Log.w (TAG, "Invalid number provided for weight: " + weightAttr.getTextContent ());
			}
		}
		NodeList children = questionNode.getChildNodes ();
		for (int i = 0; i < children.getLength (); i++)
		{
			Node child = children.item (i);
			switch (child.getNodeName ())
			{
				case "type":
					String value = child.getTextContent ();
					if (value.equals ("multi-select"))
					{
						question.type = QuestionType.MULTI_SELECT;
					}
					else if (value.equals ("single-answer"))
					{
						question.type = QuestionType.SINGLE_ANSWER;
					}
					else
					{
						Log.w (TAG, "Unrecognized question type: " + value);
					}
					break;
				case "text":
					question.text = child.getTextContent ();
					break;
				case "options":
					loadAnswerOptions (question.options, zipFile, quizPath, child);
					break;
			}
		}
		return question;
	}

	/**
	 * Loads the answer options for a question on the quiz.
	 * 
	 * @param options List where to add the loaded answer options to.
	 * @param zipFile ZIP file with the course.
	 * @param quizPath Path to this quiz within the ZIP file.
	 * @param optionsNode DOM node with the answer options to load.
	 */
	private static void loadAnswerOptions (Vector<AnswerOption> options, ZipFile zipFile,
										   String quizPath, Node optionsNode)
	{
		NodeList children = optionsNode.getChildNodes ();
		for (int i = 0; i < children.getLength (); i++)
		{
			Node child = children.item (i);
			if (child.getNodeName ().equals ("option"))
			{
				AnswerOption option = new AnswerOption ();
				Node correctAttr = child.getAttributes ().getNamedItem ("correct");
				if (correctAttr != null)
				{
					if (correctAttr.getTextContent ().trim ().equalsIgnoreCase ("true"))
					{
						option.correct = true;
					}
				}
				option.text = child.getTextContent ();
				options.add (option);
			}
		}
	}
	
}

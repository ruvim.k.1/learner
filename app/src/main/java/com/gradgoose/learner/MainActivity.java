/*
 * File: MainActivity.java
 * Author: Ruvim Kondratyev
 * Date: Sat, 17 Jul 2021
 *
 * Copyright 2021 by Ruvim Kondratyev.
 *
 * Written on Saturday, July 17, 2021.
 * The point of this was to create a
 * demo that can be used for applying
 * to a software job with a company
 * that writes eLearning software and
 * uses SCORM, xAPI, and other
 * standards as well as helps its
 * customers adopt the standards.
 *
 */
package com.gradgoose.learner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * User story:
 * As a student, I need the app to show me a list of my courses so that I can switch between courses easily.
 * 
 * This activity implements that user story.
 * 
 */
public class MainActivity extends AppCompatActivity
{
	/**
	 * The tag used for logging.
	 */
	static final String TAG = "MainActivity";

	/**
	 * The buffer size to use for copying files when extracting the sample course from the app assets.
	 */
	static final int COPY_BUFFER_SIZE = 4096;

	/**
	 * Text file that contains a LF-separated list of course file paths (one per line).
	 * 
	 * We're not really using this right now, but it's here to say that we can do this.
	 * 
	 */
	File courseListFile = null;
	/**
	 * The list of courses that we have read from that course list file.
	 */
	List<String> courseList = null;

	/**
	 * The view that lists these courses.
	 */
	RecyclerView rvCourses = null;

	/**
	 * The view-holder that shows the course item.
	 */
	class ViewHolder extends RecyclerView.ViewHolder
	{
		/**
		 * Label for showing the course name.
		 */
		TextView tvLabel;
		/**
		 * Course's position in the course list.
		 */
		int boundPosition;

		/**
		 * Initializes the view-holder by grabbing references and registering listeners.
		 * 
		 * @param itemView The root view for this item.
		 */
		public ViewHolder (@NonNull View itemView)
		{
			super (itemView);
			tvLabel = itemView.findViewById (R.id.tvLabel);
			itemView.setOnClickListener ((v) -> openCourse (courseList.get (boundPosition)));
		}

		/**
		 * Binds the view to this course.
		 * 
		 * @param position The course's position in the list of courses.
		 */
		public void bind (int position)
		{
			tvLabel.setText (new File (courseList.get (position)).getName ());
			boundPosition = position;
		}
	}

	/**
	 * Called by Android when this activity is being created or is being restarted
	 * due to a screen orientation change or some other cause.
	 *
	 * @param savedInstanceState The saved instance state to restore after the change.
	 *                           We don't use this for the demo app, but we would use
	 *                           this in a production app.
	 */
	@Override
	protected void onCreate (Bundle savedInstanceState)
	{
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_main);
		
		courseListFile = new File (Environment.getDataDirectory (), "courses.txt");
		try
		{
			courseList = Files.readAllLines (courseListFile.toPath ());
		}
		catch (IOException err)
		{
			/*
			  Note: Ideally, we'd want a mechanism to add courses
			  to this list. However, since I'm out of time (only
			  had one day to put this demo together), I'll just
			  hard-code it for the demo. I agree, it's ugly.
			 */
			courseList = new ArrayList<> ();
			File sampleCourse = new File (getFilesDir (), "Rustici-101.course");
			if (!sampleCourse.exists ())
			{
				/*
				  Here, we load the course - a slow file operation! - from the user
				  thread. Bad practice, and I wouldn't do this in a production app.
				  Just a quick demo where I only had one day to write something up.
				 */
				try
				{
					InputStream inputStream = getResources ().openRawResource (R.raw.rustici_101);
					OutputStream outputStream = new FileOutputStream (sampleCourse);
					byte [] buffer = new byte [COPY_BUFFER_SIZE];
					while (inputStream.available () > 0)
					{
						int bRead = inputStream.read (buffer);
						outputStream.write (buffer, 0, bRead);
					}
					outputStream.close ();
					inputStream.close ();
				}
				catch (IOException e)
				{
					Log.e (TAG, "Could not open sample course resource");
				}
			}
			if (sampleCourse.exists ())
			{
				courseList.add (sampleCourse.getPath ());
			}
		}
		
		rvCourses = findViewById (R.id.rvCourses);
		rvCourses.setLayoutManager (new LinearLayoutManager (this, RecyclerView.VERTICAL, false));
		rvCourses.setAdapter (new RecyclerView.Adapter<ViewHolder> ()
		{
			@NonNull
			@Override
			public ViewHolder onCreateViewHolder (@NonNull ViewGroup parent, int viewType)
			{
				View view = LayoutInflater.from (MainActivity.this).inflate (R.layout.item, parent, false);
				return new ViewHolder (view);
			}

			@Override
			public void onBindViewHolder (@NonNull ViewHolder holder, int position)
			{
				holder.bind (position);
			}

			@Override
			public int getItemCount ()
			{
				return courseList != null ? courseList.size () : 0;
			}
		});
	}

	/**
	 * Opens a course.
	 * 
	 * Will launch the CourseActivity for the course in question.
	 * 
	 * @param coursePath The path to the course file (the ZIP file with course contents).
	 */
	void openCourse (String coursePath)
	{
		Log.i (TAG, "Opening course " + coursePath);
		Intent intent = new Intent (MainActivity.this, CourseActivity.class);
		intent.setData (Uri.parse (coursePath));
		startActivity (intent);
	}
}

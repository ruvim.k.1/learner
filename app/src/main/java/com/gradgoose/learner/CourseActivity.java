/*
 * File: CourseActivity.java
 * Author: Ruvim Kondratyev
 * Date: Sat, 17 Jul 2021
 *
 * Copyright 2021 by Ruvim Kondratyev.
 *
 * Written on Saturday, July 17, 2021.
 * The point of this was to create a
 * demo that can be used for applying
 * to a software job with a company
 * that writes eLearning software and
 * uses SCORM, xAPI, and other
 * standards as well as helps its
 * customers adopt the standards.
 *
 */
package com.gradgoose.learner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

/**
 * User story:
 * As a student, I need the app to show me a list of books and quizzes that I have access to
 * so that I can learn and pass the course.
 * 
 * This activity implements that user story.
 * 
 */
public class CourseActivity extends AppCompatActivity
{
	/**
	 * The tag used for logging.
	 */
	static final String TAG = "CourseActivity";

	/**
	 * The .course file (a ZIP file with course contents inside it).
	 */
	File courseFile = null;
	/**
	 * The course content loaded into memory.
	 *
	 * (Just text though; we never load images into memory unless we're actively displaying them.)
	 *
	 */
	Util.Course course = null;

	/**
	 * View that contains the course items (books and quizzes).
	 */
	RecyclerView rvItems = null;

	/**
	 * Holder for the view.
	 * 
	 * Ideally, we'd use two view-holder types, one for books
	 * and one for quizzes, or perhaps one type but use two
	 * different icons for each item type, but in this case I
	 * just rushed this and made it all one icon. This can
	 * obviously be made more pretty later if needed, and the
	 * graphic designers are likely to change their mind about
	 * what's "pretty" to them anyway, so we won't bother at
	 * the moment.
	 * 
	 * The view-holder's responsibility is to hold a reference
	 * to the view (or views) and to bind the model to the view
	 * in a model-view-controller (MVC) type of design. They're
	 * not called MVC in Android, but it's the same concept.
	 * 
	 */
	class ViewHolder extends RecyclerView.ViewHolder
	{
		/**
		 * Item label (book name or quiz name).
		 */
		TextView tvLabel;
		/**
		 * Item itself.
		 */
		Object boundItem;
		/**
		 * Item position within the course.
		 */
		int boundPosition;

		/**
		 * Initializes the view-holder by grabbing references to views and registering listeners.
		 * 
		 * @param itemView The root view for this item.
		 */
		public ViewHolder (@NonNull View itemView)
		{
			super (itemView);
			tvLabel = itemView.findViewById (R.id.tvLabel);
			itemView.setOnClickListener ((v) -> openItem (boundPosition));
		}

		/**
		 * Binds the view to this item.
		 * 
		 * @param position The item's position within the course.
		 */
		public void bind (int position)
		{
			String name = "";
			Object item = course.findItem (position);
			if (item instanceof Util.Book)
			{
				name = ((Util.Book) item).name;
			}
			else if (item instanceof Util.Quiz)
			{
				name = ((Util.Quiz) item).name;
			}
			tvLabel.setText (name);
			boundItem = item;
			boundPosition = position;
		}
	}

	/**
	 * Called by Android when this activity is being created or is being restarted
	 * due to a screen orientation change or some other cause.
	 * 
	 * @param savedInstanceState The saved instance state to restore after the change.
	 *                           We don't use this for the demo app, but we would use
	 *                           this in a production app.
	 */
	@Override
	protected void onCreate (Bundle savedInstanceState)
	{
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_course);
		
		rvItems = findViewById (R.id.rvItems);
		rvItems.setLayoutManager (new LinearLayoutManager (this, RecyclerView.VERTICAL, false));
		rvItems.setAdapter (new RecyclerView.Adapter<ViewHolder> ()
		{
			@NonNull
			@Override
			public ViewHolder onCreateViewHolder (@NonNull ViewGroup parent, int viewType)
			{
				View view = LayoutInflater.from (CourseActivity.this).inflate (R.layout.item, parent, false);
				return new ViewHolder (view);
			}

			@Override
			public void onBindViewHolder (@NonNull ViewHolder holder, int position)
			{
				holder.bind (position);
			}

			@Override
			public int getItemCount ()
			{
				return course != null ? course.countItems () : 0;
			}
		});

		Uri data = getIntent ().getData ();
		courseFile = new File (data.getPath ());
		if (!courseFile.exists ())
		{
			Toast.makeText (this, "Course file does not exist", Toast.LENGTH_SHORT).show ();
			finish ();
			return;
		}

		loadCourse ();
	}

	/**
	 * Loads the course content.
	 * 
	 * Obviously, we never load images into memory on Android
	 * due to memory limitations.
	 * 
	 */
	void loadCourse ()
	{
		course = Util.loadCourse (courseFile);
		setTitle (course.name);
	}

	/**
	 * Opens a course item.
	 * 
	 * Will start either the BookReader or the QuizTaker activity depending on the item.
	 * 
	 * @param position The item's position in the course.
	 */
	void openItem (int position)
	{
		Object item = course.findItem (position);
		Intent intent = null;
		if (item instanceof Util.Book)
		{
			intent = new Intent (CourseActivity.this, BookReader.class);
		}
		else if (item instanceof Util.Quiz)
		{
			intent = new Intent (CourseActivity.this, QuizTaker.class);
		}
		if (intent != null)
		{
			intent.setData (Uri.fromFile (courseFile));
			intent.putExtra ("item", position);
			startActivity (intent);
		}
	}
	
}

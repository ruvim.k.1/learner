/*
 * File: QuizTaker.java
 * Author: Ruvim Kondratyev
 * Date: Sat, 17 Jul 2021
 *
 * Copyright 2021 by Ruvim Kondratyev.
 *
 * Written on Saturday, July 17, 2021.
 * The point of this was to create a
 * demo that can be used for applying
 * to a software job with a company
 * that writes eLearning software and
 * uses SCORM, xAPI, and other
 * standards as well as helps its
 * customers adopt the standards.
 *
 */
package com.gradgoose.learner;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Vector;

/**
 * User story:
 * As a teacher, I need my students to be able to take a quiz on the course content so that they can learn better.
 * 
 * User story:
 * As a teacher, I need my students' score reports so that I can assign grades and help the struggling students.
 * 
 * This activity implements these user stories.
 * 
 */
public class QuizTaker extends AppCompatActivity
{
	/**
	 * The tag used for logging.
	 */
	static final String TAG = "CourseActivity";

	/**
	 * The .course file (a ZIP file with course contents inside it).
	 */
	File courseFile = null;
	/**
	 * The course content loaded into memory.
	 *
	 * (Just text though; we never load images into memory unless we're actively displaying them.)
	 *
	 */
	Util.Course course = null;

	/**
	 * The number (or index, or position) of the item that we're reading.
	 */
	int itemPosition = 0;
	/**
	 * The item that we're reading.
	 */
	Object item = null;

	/**
	 * The Quiz that we're taking (same as item, but cast to a Quiz if it's a Quiz).
	 */
	Util.Quiz quiz = null;
	/**
	 * Whether or not we should be showing the correct answers.
	 * 
	 * This flag turns to 'true' once the 'submit quiz' button has been clicked.
	 * 
	 */
	boolean showGrade = false;

	/**
	 * View that shows all the quiz questions.
	 */
	RecyclerView rvQuestions = null;
	/**
	 * View that shows a grade report to the user.
	 */
	TextView tvGradeReport = null;

	/**
	 * View-holder for each question in the quiz.
	 */
	class ViewHolder extends RecyclerView.ViewHolder
	{
		/**
		 * Element to hold the text of the question.
		 */
		final TextView tvQuestion;
		/**
		 * Element to hold answer option buttons.
		 */
		final RadioGroup rgOptions;

		/**
		 * Our list of answer option buttons.
		 * 
		 * These can be RadioButton or CheckBox, but
		 * either way will extend CompoundButton, so
		 * we can treat them the same regardless.
		 * 
		 */
		final Vector<CompoundButton> answerButtons = new Vector<> ();

		/**
		 * The question this is bound to.
		 */
		Util.QuizQuestion question;
		/**
		 * The list position this is bound to.
		 */
		int boundPosition;

		/**
		 * Initializes the view-holder by grabbing references to views.
		 * 
		 * @param itemView The root view for this question.
		 */
		public ViewHolder (@NonNull View itemView)
		{
			super (itemView);
			tvQuestion = itemView.findViewById (R.id.tvQuestion);
			rgOptions = itemView.findViewById (R.id.rgOptions);
		}

		/**
		 * Binds this view to the position in the list / to the question.
		 * 
		 * @param position The position in the list.
		 */
		public void bind (int position)
		{
			boundPosition = position;
			question = quiz.questions.elementAt (position);
			
			tvQuestion.setText (question.text);
			
			rgOptions.removeAllViews ();
			answerButtons.clear ();
			
			@LayoutRes int layoutId;
			@IdRes int buttonId;
			if (question.type.equals (Util.QuestionType.SINGLE_ANSWER))
			{
				// Radio button type of question.
				layoutId = R.layout.answer_option_radio;
				buttonId = R.id.rbOption;
			}
			else
			{
				// Select all that apply (checkbox) type of question.
				layoutId = R.layout.answer_option_checkbox;
				buttonId = R.id.cbOption;
			}
			
			for (int i = 0; i < question.options.size (); i++)
			{
				final Util.AnswerOption option = question.options.elementAt (i);
				View view = LayoutInflater.from (QuizTaker.this).inflate (layoutId, rgOptions, false);
				View button = view.findViewById (buttonId);
				
				button.setId (View.generateViewId ());
				
				if (button instanceof CompoundButton)
				{
					CompoundButton compoundButton = (CompoundButton) button;
					compoundButton.setText (option.text);
					compoundButton.setChecked (option.userSelected);
					compoundButton.setOnCheckedChangeListener ((v, value) -> {
						option.userSelected = value;
					});
					
					if (showGrade)
					{
						boolean optionCorrect = (option.userSelected == option.correct);

						if (optionCorrect)
						{
							compoundButton.setTextColor (getResources ().getColor (R.color.answer_correct));
						}
						else
						{
							compoundButton.setTextColor (getResources ().getColor (R.color.answer_wrong));
						}
					}
					else
					{
						compoundButton.setTextColor (getResources ().getColor (R.color.black));
					}
					
					answerButtons.add (compoundButton);
				}
				
				rgOptions.addView (view);
			}
		}
	}

	/**
	 * Called by Android when this activity is being created or is being restarted
	 * due to a screen orientation change or some other cause.
	 *
	 * @param savedInstanceState The saved instance state to restore after the change.
	 *                           We don't use this for the demo app, but we would use
	 *                           this in a production app.
	 */
	@Override
	protected void onCreate (Bundle savedInstanceState)
	{
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_quiz_taker);

		Uri data = getIntent ().getData ();
		courseFile = new File (data.getPath ());
		if (!courseFile.exists ())
		{
			Toast.makeText (this, "Course file does not exist", Toast.LENGTH_SHORT).show ();
			finish ();
			return;
		}

		Bundle extras = getIntent ().getExtras ();
		if (extras != null)
		{
			itemPosition = extras.getInt ("item");
		}

		course = Util.loadCourse (courseFile);
		item = course.findItem (itemPosition);
		if (item instanceof Util.Quiz)
		{
			quiz = ((Util.Quiz) item);
			/*
			  Note on randomize: for this demo, it doesn't matter, but
			  for an actual application for a customer, we'd need to
			  persist the randomized list somewhere and reload it from
			  there onCreate () if available because on Android, when
			  the screen flips from vertical to horizontal or vice
			  versa, the activity is RESTARTED! That's why we make use
			  of onSaveInstanceState () and onRestoreInstanceState ()
			  or onCreate (). If we randomize on every onCreate () then
			  that would result in the list being randomized every time
			  the user turns the phone's or tablet's orientation
			  sideways. Just a heads-up, this is only a demo app for now!
			 */
			quiz.randomize ();
			setTitle (quiz.name);
		}
		else
		{
			quiz = null;
		}

		rvQuestions = findViewById (R.id.rvQuestions);
		rvQuestions.setLayoutManager (new LinearLayoutManager (this, RecyclerView.VERTICAL, false));
		rvQuestions.setAdapter (new RecyclerView.Adapter<ViewHolder> ()
		{
			@NonNull
			@Override
			public ViewHolder onCreateViewHolder (@NonNull ViewGroup parent, int viewType)
			{
				View view = LayoutInflater.from (QuizTaker.this).inflate (R.layout.question, parent, false);
				return new ViewHolder (view);
			}

			@Override
			public void onBindViewHolder (@NonNull ViewHolder holder, int position)
			{
				holder.bind (position);
			}

			@Override
			public int getItemCount ()
			{
				return quiz != null ? quiz.questions.size () : 0;
			}
		});

		tvGradeReport = findViewById (R.id.tvGradeReport);

		findViewById (R.id.btnSubmit).setOnClickListener ((v) -> gradeQuiz ());
	}

	/**
	 * Grades the quiz.
	 * 
	 * Firstly, this causes the question answer options to become
	 * color-coded on whether or not they were answered correctly.
	 * 
	 * Secondly, this produces an overall grade report to the user.
	 * 
	 */
	void gradeQuiz ()
	{
		if (quiz != null)
		{
			showGrade = true;
			rvQuestions.getAdapter ().notifyDataSetChanged (); // Will change colors to red, green, etc.

			Util.GradeReport report = quiz.grade ();
			tvGradeReport.setText (
					getString (R.string.report_graded)
					.replace ("[EARNED]", String.valueOf (report.earned))
					.replace ("[MIN]", String.valueOf (report.min))
					.replace ("[MAX]", String.valueOf (report.max))
					.replace ("[SCALED]", String.valueOf (report.scaled))
			);
		}
	}
}


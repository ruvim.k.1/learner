<!DOCTYPE html>
<!--
    INSTRUCTIONS:

    Here's a small problem that a client once asked us to solve as part of a larger project.
    The client has an existing web-based system for delivering a test containing multiple choice
    questions to a learner (the relevant parts of which are included on this page). The client
    asked us to modify this code so that the test questions are delivered in a random order every
    time a learner attempts the test. Furthermore, the order in which the answers to the questions
    are presented should also be randomized.

    The existing framework for creating and displaying a test has been provided for you below. Your
    task is to comprehend the client's code and data structure then implement the `randomizeTest`
    function to perform the randomization of the question and answer order. Please explain your work
    and thought process.

    Some background on the project from the client that may affect your solution:

    - There are over 600 of these tests deployed to hundreds of thousands of users
    - There are never more then 20 questions or so per test, each with no more than 6 answers but the
      code should be able to handle an arbitrary number of both questions and answers
    - This code is maintained by several developers in different organizations
    - The code is only required to work in all modern browsers

    When submitting your response, please rename this file to include your name.

    If you have any questions, please do not hesitate to ask.


************************************************ RUVIM'S RESPONSE *****************************************************

Answer By: Ruvim Kondratyev
Date: Sun, 18 Jul 2021

----

The first thing to see about this program is the randomizeTest () function. It takes
a Test object and returns a Test object, as the comment already tells us.

From reading the code, it is evident that the Test object contains 'questions' (an array
of strings, one string per question), 'choices' (an array of arrays of strings, where
there is one subarray per question, and each subarray is a list of answer options), and
'answers' (an array of arrays of integers which are all either 1 or 0 depending on which
answer choices are correct and which are not correct). The 'answers' has all its elements
and subarray elements correspond to 'choices', so there is one element (1 or 0) per
answer choice in the subarrays, and one subarray per question. Our code modifications
must preserve this data structure.

Per the wording of the task ("the questions in the returned object should be in a random
order"), the "in the returned object" suggests that we leave the original object in-tact.
JavaScript is a copy-by-reference language, so we will need to instantiate a new Test and
copy items over in a deep-copy fashion in order to preserve the original object.

Once we make the copy, we need to randomize the copy. There are a few approaches we can
in theory think of:
A. Run JavaScript's Array.sort (), giving it a comparator function that returns random.
B. Copy elements from a random input array position to an orderly output array position.
C. Copy elements from an orderly input array position to a random output array position.
D. Randomize using swap operations while swapping items to the end of the array.
E. Randomize using swap operations while swapping items to the beginning of the array.

For option A,
- Advantage: It is simple to code: items.sort (function (a, b) { return Math.random () - 0.5; });
- Disadvantage: It is complex, with a complexity of O(N log N) or more, depending on the sort algorithm.

For options B and C,
- Advantage: Does not require copying the array first, but that's about it. No real advantages.
- Disadvantages:
- - Requires keeping a map of which input or output indices have been used up already.
- - The very first copy is O(1), but the rest are more and more complex, so overall it is O(N^2).
- - This is just too complicated.
- UNLESS, we impose an optimization to make the randomness still be orderly as in options D and E.

Options D and E are effectively the optimized version of option B where we copy from
random input positions to an orderly output position (the output position starting from
the array's end and working toward the beginning in the case of option D, or starting
from the array's beginning and working toward the end in the case of option E). But we
confine the randomness of the input position to only cover what hasn't been copied yet,
so in that sense it's still random, but we optimized it for the special case where we
only ever randomize the array by swapping to the beginning or the end of the array, so
this way we always have the remainder of the array as a contiguous range of indices from
which we can draw our next random number such that we don't have to maintain a map of
indices that were "used up" but simply the remaining range of indices is what we are
left with to use.

For options D and E,
- Advantage: The complexity is O(N), so it'll be the fastest of these and most scalable.
- Disadvantage: It's not complicated at all, but it's a little bit more than just a Math.random ().

The difference between randomizing-swapping to the end or the beginning is really just in the equations.
Personally, I like option D better because the equation will require slightly less cycles to compute,
but it's only a small difference, and the complexity is what's more important, which is O(N) for both
options D and E.

The part that makes this slightly more complicated is the fact that there's multiple arrays that
all need to be randomized together in a way such that parallel elements are still parallel to each
other after the randomization is done (think: 'choices' is parallel to 'answers'). To overcome this,
we will consider first building an array of indices for the swap algorithm, then use these indices
to perform the actual swapping - thus separating our randomization algorithm into two steps. The
output from the first step is just a pattern of how to randomize the array while the second step can
reuse this pattern as many times as needed to randomize as many parallel arrays as needed.

For this, we will create two functions:
- createRandomPattern () will create this pattern of indices for randomizing an array of this length.
- applyRandomPattern () will apply this particular pattern to an array.

Then we just need to do the following in randomizeTest ():
- Create a new Test, and do a deep copy of the old test's contents into this new instance.
- Call createRandomPattern () to generate a random sequence for 'questions.length'.
- Call applyRandomPattern (), passing it the pattern and the 'questions'.
- Call applyRandomPattern (), passing it the pattern and the 'choices'.
- Call applyRandomPattern (), passing it the pattern and the 'answers'.
- For each question:
- - Call createRandomPattern () to generate a random sequence for 'choices[i].length'.
- - Call applyRandomPattern (), passing it the pattern and the 'choices[i]'.
- - Call applyRandomPattern (), passing it the pattern and the 'answers[i]'.

The above will randomize all questions and answer options, and it will be scalable because it's O(N).

I've used Doxygen-style docs for short parameter references, where @p param refers to
a parameter whose name is 'param', and @c item refers to a piece of code (such as a
variable name) for 'item'.

Style note: I prefer function calls to have spaces as 'Math.random ()', but, when working
with customers, it's important to keep their existing code style, so I respected that and
used 'Math.random()' instead of 'Math.random ()', etc. Please forgive me if I accidentally
do it "my way" as I'm so used to typing.


-------------------- Side note: ---------------------------

Here is another demo of my work:
https://gitlab.com/ruvim.k.1/learner

And its corresponding demo video:
https://www.youtube.com/watch?v=IRUkzGrp2gk


-------------------- Test results: ------------------------

To test if our algorithm truly does randomize the arrays, we
need to call it many times on many arrays and create a histogram
of its output. For this, I created #testRandomnessOfPattern ()
to make this histogram. If the output of the algorithm really is
random, then we expect a flat histogram where all the numbers are
approximately the same (with some variations due to this being
discrete, but they'll be the same order of magnitude).

Testing this, I find that the numbers in every cell of the
histogram are all the same order of magnitude, and this
observation is in agreement with the hypothesis that the
algorithm really will randomize the array.

The following is the pretty-print of a call to:
>> copy (JSON.stringify (testRandomnessOfPattern (10 * 1024, 8)))

Test result:
[
  [
    1332,
    1300,
    1257,
    1278,
    1256,
    1269,
    1274,
    1274
  ],
  [
    1320,
    1283,
    1305,
    1292,
    1277,
    1218,
    1265,
    1280
  ],
  [
    1260,
    1297,
    1278,
    1296,
    1225,
    1318,
    1301,
    1265
  ],
  [
    1326,
    1269,
    1205,
    1299,
    1305,
    1278,
    1312,
    1246
  ],
  [
    1335,
    1279,
    1290,
    1242,
    1245,
    1319,
    1253,
    1277
  ],
  [
    1183,
    1275,
    1346,
    1260,
    1319,
    1254,
    1291,
    1312
  ],
  [
    1241,
    1312,
    1234,
    1303,
    1276,
    1314,
    1269,
    1291
  ],
  [
    1243,
    1225,
    1325,
    1270,
    1337,
    1270,
    1275,
    1295
  ]
]

These numbers are all close to each other in magnitude, so this is
in agreement with what we expect.

-->
<html lang="en">
<head>
    <title>Rustici Software - Ruvim Kondratyev - Initial Developer Hiring Test</title>
    <style>
        body {
            font-family: sans-serif;
            font-size: 14px;
            line-height: 20px;
        }
        li.question {
            padding-top: 5px;
            padding-bottom: 5px;
        }
        label.choice {
            display: block;
        }
        .correct {
            color: #336897;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript">
        /**
         * Creates a pattern of random indices to randomize an array of length @p N.
         * 
         * Algorithm:
         * 1. Create the swap indices using our end-to-beginning formula of (i * random).
         * 
         * @author Ruvim Kondratyev
         * @date Sun, 18 Jul 2021
         * 
         * @param N The number of elements to create random indices for.
         * 
         * @return An array of indices. The length of this array will be N-1.
         */
        function createRandomPattern (N) {
            pattern = [];
            for (let i = N; i > 1; i--) {
                pattern.push(Math.floor(i * Math.random()));
            }
            return pattern;
        }
        /**
         * Applies a pattern of indices to randomize an array using element swap operations.
         * 
         * Algorithm:
         * 1. For each element in the pattern, swap that index to build end-to-beginning randomly.
         * 
         * The equation for @c a here is as such:
         * - For pattIdx = 0, we want the last element of the @p subject array.
         * - The @p subject is one element longer than the @p pattern.
         * - Thus, for pattIdx = 0, the @c a is just @c pattern.length.
         * - For pattIdx = 1, we want the element before that, so @c pattern.length-1.
         * - Thus, it's @c pattern.length-pattIdx for determining @c a.
         * 
         * @author Ruvim Kondratyev
         * @date Sun, 18 Jul 2021
         * 
         * @param pattern The indices to use to build the result array. Use #createRandomPattern () for random.
         * @param subject The source array to 
         */
        function applyRandomPattern (pattern, subject) {
            for (let pattIdx = 0; pattIdx < pattern.length; pattIdx++) {
                let a = pattern.length - pattIdx;
                let b = pattern[pattIdx];
                let tmp = subject[a];
                subject[a] = subject[b];
                subject[b] = tmp;
            }
        }
        /**
         * Function to test how random the pattern is.
         * 
         * This is more of a qualitative test, but it returns the histogram
         * of the sum of all the possibilities that have been observed by
         * running #createRandomPattern () and #applyRandomPattern () many
         * times.
         * 
         * If the randomization truly is random, then we expect a flat
         * distribution of the return of this function. If we see any
         * peaks or valleys, then that is a sign that we did the randomization
         * wrong.
         * 
         * @author Ruvim Kondratyev
         * @date Sun, 18 Jul 2021
         * 
         * @param n How many times to try this (this is the number of vectors we sum over).
         * @param N How many elements to try (this is the number of histogram bins).
         * 
         * @return The histogram 2D array, each element representing the count of that element's randomization.
         */
        function testRandomnessOfPattern (n, N) {
            let hist = [];
            // 1. Init all 0s.
            for (let j = 0; j < N; j++) {
                hist[j] = [];
                for (let k = 0; k < N; k++) {
                    hist[j][k] = 0;
                }
            }
            // 2. Do randomness @p n times.
            for (let i = 0; i < n; i++) {
                // A. Create pattern.
                let pattern = createRandomPattern(N);
                // B. Create an orderly array.
                let testArray = [];
                for (let j = 0; j < N; j++) {
                    testArray[j] = j;
                }
                // C. Randomize it using the pattern.
                applyRandomPattern(pattern, testArray);
                // D. Update the histogram counts.
                for (let j = 0; j < N; j++) {
                    let k = testArray[j];
                    hist[j][k]++;
                }
            }
            // Done.
            return hist;
        }
    </script>
</head>

<body>
    <h2>Randomized Questions</h2>
    <ul id="questions"></ul>

    <script>
        (function () {
            //
            // `randomizeTest` accepts and returns a `Test` object. The questions in the returned object should
            // be in a random order. The order of the choices within each question should also be randomized.
            //
            function randomizeTest (test) {
                let result = new Test ([], [], []);
                // 1. Deep copy.
                for (let i = 0; i < test.questions.length; i++) {
                    result.questions[i] = test.questions[i];
                    result.choices[i] = [];
                    result.answers[i] = [];
                    for (let j = 0; j < test.choices[i].length; j++) {
                        result.choices[i][j] = test.choices[i][j];
                        result.answers[i][j] = test.answers[i][j];
                    }
                }
                // 2. Randomize questions.
                let pattern = createRandomPattern(test.questions.length);
                applyRandomPattern(pattern, result.questions);
                applyRandomPattern(pattern, result.choices);
                applyRandomPattern(pattern, result.answers);
                // 3. Randomize answer options.
                for (let i = 0; i < result.questions.length; i++) {
                    let subPattern = createRandomPattern(result.choices[i].length);
                    applyRandomPattern(subPattern, result.choices[i]);
                    applyRandomPattern(subPattern, result.answers[i]);
                }
                // Done.
                return result;
            }

            function Test (questions, choices, answers) {
                this.questions = questions;
                this.choices = choices;
                this.answers = answers;
            }

            //
            // displays the sample test in the browser with the correct answer highlighted
            //
            function renderTest (test, parent) {
                const randomizedTest = randomizeTest(test);

                for (let i = 0; i < randomizedTest.questions.length; i += 1) {
                    const qElement = document.createElement("li");
                    let correctCount = 0;

                    qElement.setAttribute("class", "question");
                    qElement.appendChild(
                        document.createTextNode(randomizedTest.questions[i])
                    );

                    for (let j = 0; j < randomizedTest.answers[i].length; j += 1) {
                        if (randomizedTest.answers[i][j] === 1) {
                            correctCount += 1;
                        }
                    }

                    for (let j = 0; j < randomizedTest.choices[i].length; j += 1) {
                        const choiceLabelElement = document.createElement("label"),
                            choiceInputElement = document.createElement("input");

                        choiceInputElement.setAttribute("name", (correctCount === 1 ? "radio" : "check") + i);
                        choiceInputElement.setAttribute("type", correctCount === 1 ? "radio" : "checkbox");
                        choiceInputElement.setAttribute("value", j);

                        choiceLabelElement.classList.add("choice");
                        if (randomizedTest.answers[i][j] === 1) {
                            choiceLabelElement.classList.add("correct");
                        }

                        choiceLabelElement.appendChild(choiceInputElement);
                        choiceLabelElement.appendChild(
                            document.createTextNode(randomizedTest.choices[i][j])
                        );

                        qElement.appendChild(choiceLabelElement);
                        parent.appendChild(qElement);
                    }
                }
            }

            const questions = [
                    "What can you find in Rustici Software's office?",
                    "All of Rustici Software employees are expected to work no more than ____ hours per week.",
                    "The end users of Rustici Software's products number in the _________",
                    "Rustici Software is a (choose all that apply):",
                    "Tim likes to wear:"
                ],
                choices = [
                    [
                        "Dart Board",
                        "Ping Pong Table",
                        "Cubicles",
                        "Laptops with dual monitors",
                        "TPS reports, ummm yeah"
                    ],
                    [
                        "80",
                        "40",
                        "50",
                        "60"
                    ],
                    [
                        "Tens",
                        "Hundreds",
                        "Thousands",
                        "Millions",
                        "Billions"
                    ],
                    [
                        "Great place to work",
                        "Respected leader in its field",
                        "Place where people don't matter, just results"
                    ],
                    [
                        "Capri pants",
                        "Goth attire",
                        "Sport coat",
                        "T-shirt and shorts"
                    ]
                ],
                answers = [
                    [1, 1, 0, 1, 0],
                    [0, 1, 0, 0],
                    [0, 0, 0, 1, 0],
                    [1, 1, 0],
                    [0, 0, 0, 1, 0]
                ],
                test = new Test(questions, choices, answers);

            renderTest(test, document.getElementById("questions"));
        }());
    </script>
</body>
</html>
